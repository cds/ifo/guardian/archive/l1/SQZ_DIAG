#import time
#import cdslib
#Using the SYS_DIAG from the main guardian.
# All nominal numbers are from March 2019 when generating 3dB squeezing.

from SYS_DIAG import (SYSDIAG, request, nominal, INIT, RUN_TESTS, edges)
import cdsutils
import time

if False:
    #this stub prevents pylint from complaining for Lee. M
    ezca = None



@SYSDIAG.register
def SQZ_SHG():
    """Check if SHG power is enough
    """
    target = 50
    val = ezca['SQZ-SHG_GR_DC_POWERMON']

    if ezca['GRD-SQZ_SHG_STATE_N'] == 50 and val < target:
        yield "SHG generates less than 50 mW. Try optimize SHG alignment or TEC"



@SYSDIAG.register
def SQZ_GRN_FIBR_POWER():
    """Green input power check
    for protecting the green fiber

    """
    grn_input = ezca['SQZ-SHG_LAUNCH_DC_POWERMON']

    if grn_input > 30:
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 0
        yield "Green power input to OPO is over 20mW! Flipper closed. Adjust pol or fiber alignment"




@SYSDIAG.register
def SQZ_GRN_FIBR_FLIPPER():
    """Check whether the green fiber
    flipper was closed.
    """
    if ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] == 0:
        yield "SHG fiber flipper closed."


@SYSDIAG.register
def SQZ_OPO_TRANS():
    """Check OPO TRANS power when locked
    """
    if ezca['GRD-SQZ_OPO_STATE_N'] == 50 and ezca['SQZ-OPO_TRANS_LF_OUTPUT'] < 0.06:
        yield "OPO TRANS power too low. Check OPO TEC or pump power."


@SYSDIAG.register
def SQZ_GRN_ISS():
    """Check OPO ISS working or not
    """
    target = ezca['SQZ-SHG_GRD_SLOW_TARGET']
    val = ezca['SQZ-OPO_TRANS_LF_OUTPUT']
    gr_open = ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']
    tol = 0.01
    if gr_open and (abs(val - target) > tol):
        yield "OPO TRANS too low. Check pump fiber alignment or adjust pol"




@SYSDIAG.register
def SQZ_TTFSS_EOM():
    """Check whether the TTFSS
        EOM is railed.
    """
    if abs(ezca['SQZ-FIBR_EOMRMS_OUTPUT']) > 32000:
        yield "TTFSS EOM is railed, run the TTFSS LOCKLOSS state"


@SYSDIAG.register
def SQZ_CLF_SHUTTER():
    """Check whether the CLF
    shutter was closed.
    """
    if ezca['SYS-MOTION_C_SHUTTER_I_STATE'] == 1:
        yield "CLF fast shutter closed (needed to lock OPO)"


@SYSDIAG.register
def SQZ_CLF_ISS():
    """Check CLF ISS working or not
    """
    target = ezca['SQZ-CLF_GRD_RF_TARGET']
    val = ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON']
    tol = 1
    if ezca['GRD-SQZ_CLF_STATE_N'] == 1000 and abs(val - target) > tol:
        yield "CLF REFL RF6 beatnote too low. Check CLF fiber alignment or OPO TEC"

    target = ezca['SQZ-CLF_ISS_DC_TARGET']
    val = ezca['SQZ-CLF_REFL_RF6_ABS_OUTPUT']
    tol = 0.5
    if ezca['GRD-SQZ_CLF_STATE_N'] == 1000 and abs(val - target) > tol:
        yield "CLF REFL DC too low. Check CLF fiber alignment or OPO TEC"




'''
@SYSDIAG.register
def SQZ_CLF_POWER():
    """CLF 2f power check

    """
    clf_2f_power = cdsutils.avg(1,'SQZ-CLF_REFL_DC_POWER')
    clf_2f_threshold = ezca['SYS-MOTION_C_SHUTTER_I_THRESHOLD']

    if clf_2f_power > clf_2f_threshold:	#was 0.007
        yield "CLF 2f Power too high. Fast shutter closed. Adjust CLF POWER pico. Nominal: 4.2e-03."

    if clf_2f_power < 0.01:
        yield "CLF 2f Power too low. Adjust CLF POWER pico. Nominal: 4.2e-03."
'''


@SYSDIAG.register
def SQZ_ZM_POS():
    """ZM Position check

    """
    POS_TOLERANCE = 200
    NOMINAL = {'P':{'ZM1':606,'ZM2':620,'ZM3':100,'ZM4':650,'ZM5':-223,'ZM6':-450,'FC1':-121,'FC2':-521}, #ZM4 1179, ZM6 -200
               'Y':{'ZM1':-2178,'ZM2':-1205,'ZM3':648,'ZM4':450,'ZM5':-886,'ZM6':514,'FC1':303,'FC2':112},} #ZM4 293, ZM6 640
               

    for dof in ['P','Y']:
        for sus in NOMINAL[dof]:
            ofs = ezca['SUS-%s_M1_OPTICALIGN_%s_OFFSET'%(sus,dof)]
            if abs(ofs-NOMINAL[dof][sus]) > POS_TOLERANCE:
                yield '%s %s nominal %d at 06012023'%(sus,dof,NOMINAL[dof][sus])




@SYSDIAG.register
def SQZ_PSAMS():
    """Check if PSAMS PZT is locked
    """
    target = ezca['AWC-ZM2_M2_STRAIN_VOLTAGE_TARGET']
    val = ezca['AWC-ZM2_PSAMS_STRAIN_VOLTAGE']
    tol = 0.5

    if ezca['GRD-SQZ_SUS_STATE_N'] == 200 and abs(val - target) > tol:
        yield "ZM2 PSAMS is not at the set point. Check PSAMS servo"

    target = ezca['AWC-ZM4_M2_STRAIN_VOLTAGE_TARGET']
    val = ezca['AWC-ZM4_PSAMS_STRAIN_VOLTAGE']
    tol = 0.1

    if ezca['GRD-SQZ_SUS_STATE_N'] == 200 and abs(val - target) > tol:
        yield "ZM4 PSAMS is not at the set point. Check PSAMS servo"

    target = ezca['AWC-ZM5_M2_STRAIN_VOLTAGE_TARGET']
    val = ezca['AWC-ZM5_PSAMS_STRAIN_VOLTAGE']
    tol = 0.05

    if ezca['GRD-SQZ_SUS_STATE_N'] == 200 and abs(val - target) > tol:
        yield "ZM5 PSAMS is not at the set point. Check PSAMS servo"





@SYSDIAG.register
def SQZ_OPO_TEC():
    """OPO Temperature Check

    """

    if ezca['SQZ-OPO_TEC_SETTEMP'] > 33 or ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 33:
        yield "OPO Temperature too high? nominal 31.37. --10272019"

    if ezca['SQZ-OPO_TEC_SETTEMP'] < 30 or ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 30:
        yield "OPO Temperature too low? nominal 31.37. --10272019"

@SYSDIAG.register
def SQZ_OPO_TEMP_OPT():
    """Reminder of Optimizing OPO Temperature

    """
    #if ezca['GRD-SQZ_MANAGER_STATE_N'] == 21 or ezca['GRD-SQZ_MANAGER_STATE_N'] == 22:
    #if ezca['GRD-SQZ_OPO_STATE_N'] == 10 and ezca['SQZ-OPO_REFL_DC_POWER'] > 1.0:
    #    if ezca['SQZ-EST_RF6_NLGDBS_OUT16'] < 8:
    #        yield "NLGDBs low, check OPO crystal temp."
    rf_target = ezca['SQZ-CLF_GRD_RF_TARGET']
    rf_val = ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON']
    rf_tol = 1
    target = ezca['SQZ-CLF_ISS_DC_TARGET']
    val = ezca['SQZ-CLF_REFL_RF6_ABS_OUTPUT']
    tol = 0.5
    if ezca['GRD-SQZ_CLF_STATE_N'] == 1000 and abs(val - target) < tol and rf_target - rf_val > rf_tol:
        yield "Check OPO TEC because CLF RF6 is too low"





'''
@SYSDIAG.register
def SQZ_SHG_SLOW():
    """SHG Slow Loop Power Target Check

    """
    if (
        (ezca['SQZ-SHG_GRD_SLOW_TARGET'] < ezca['SQZ-SHG_LAUNCH_DC_POWERMON']-1) or
        (ezca['SQZ-SHG_GRD_SLOW_TARGET'] > ezca['SQZ-SHG_LAUNCH_DC_POWERMON']+1)
    ):
        yield "Please reset SHG SLOW Power Target"
'''

'''
@SYSDIAG.register
def SQZ_OPO_SLOW():
    """OPO Slow Loop Power Target Check

    """
    if (
        (ezca['SQZ-OPO_GRD_SLOW_NLG_TARGET'] < ezca['SQZ-EST_RF6_NLGDBS_OUT16']-0.5) or
        (ezca['SQZ-OPO_GRD_SLOW_NLG_TARGET'] > ezca['SQZ-EST_RF6_NLGDBS_OUT16']+0.5)
    ):
        yield "Please reset OPO SLOW Power Target"

'''

GRN_POL_TRIG = False

@SYSDIAG.register
def SQZ_GRN_POL():
    """Green Beam Polarisation Check

    """
    #TODO divide by the DC_POWERMON to keep normalized?
    global GRN_POL_TRIG
    if not GRN_POL_TRIG:
        if ezca['SQZ-SHG_FIBR_REJECTED_DC_POWER'] > 0.2: #0.05
            GRN_POL_TRIG = True
    else:
        if ezca['SQZ-SHG_FIBR_REJECTED_DC_POWER'] < 0.195: #0.045
            GRN_POL_TRIG = False

    if GRN_POL_TRIG:
        yield "SHG_FIBER REJ too high. Adjust SHG_POL pico to min. Nominal 45e-03."

'''
@SYSDIAG.register
def SQZ_LASER_HEAD_CRYSTAL_TEMP():
    """Laser Head Crystal Temperature check
    """
    if ezca['SQZ-LASER_HEAD_CRYSTALTEMPERATURE'] > 0.0:
        yield "Check TTFSS, laser head crystal temperature setting"
'''

'''
def SQZ_GRN_FIBR_ALIGN():
    """Green input power opo refl check
    for hand-aligning the green fiber coupling

    """
    grn_input = ezca['SQZ-SHG_LAUNCH_DC_POWERMON']

    if grn_input > 19.0 and ezca['GRD-SQZ_OPO_STATE_N'] > 9 and ezca['SQZ-OPO_REFL_DC_POWER'] < 1.95:
        yield "Need Hand-tuning Green Fiber Coupling. Nominal OPO REFL 2.1e+00 with input 19.2. --03192018"
'''

'''
def OPO_SCAN_MAX_RESET():
    """ Check whether the OPO Scan Max need to be reset."""
    if ezca['GRD-SQZ_OPO_STATE_N'] == 7:
        self.timer['ScanTimer'] = 30
        if self.timer['ScanTimer']:
            yield "Manually reset OPO Scan Max in autolocker needed."
'''

@SYSDIAG.register
def SQZ_FC_105kHz():
    """Check if RLF-CLF beatnote power is enough
    """
    target = 3500
    val = ezca['SQZ-FC_WFS_A_I_SUM_OUTPUT']
    
    if ezca['GRD-SQZ_FC_STATE_N'] == 1000 and val < target:
        yield "RLF-CLF 105 kHz beatnote too low on FC WFS A. Check if FC is locked"
    
    target = 2500
    val = ezca['SQZ-FC_WFS_B_I_SUM_OUTPUT']

    if ezca['GRD-SQZ_FC_STATE_N'] == 1000 and val < target:
        yield "RLF-CLF 105 kHz beatnote too low on FC WFS B. Check if FC is locked"



@SYSDIAG.register
def SQZ_OMCRF3_and_AS42():
    """Check squeezing alignments to OMC and AS42
    """
    target = 950
    val = ezca['ASC-AS_A_RF42_SUM_NORM_OUTPUT']

    if ezca['GRD-SQZ_MANAGER_STATE_N'] == 1000 and val < target:
        yield "AS_A RF42 is lower than 1000. Check alignments to OMC. "

    target = -15
    val = ezca['SQZ-OMC_TRANS_RF3_DEMOD_RFMON']

    if ezca['GRD-SQZ_MANAGER_STATE_N'] == 1000 and val < target:
        yield "OMC RF3 is lower than %d dB. Check alignments to OMC. "%target





@SYSDIAG.register
def SQZ_dB_2kHz():
    """Check high-frequency squeezing dB
    """
    target = -2.5
    val = ezca['SQZ-DCPD_RATIO_4_DB_MON']

    if ezca['GRD-SQZ_MANAGER_STATE_N'] == 1000 and val > target:
        yield "2 kHz squeezing is less than 5 dB. Re-optimize alignments"

####################

####################
